# Webcounter Project

Simple Python Webcounter with redis server

📦webcounter
 ┣ 📂static
 ┃ ┗ 📜main.css
 ┣ 📂templates
 ┃ ┗ 📜index.html
 ┣ 📜__init__.py
 ┗ 📜__main__.py

---
## Developer tasks

### Local run

    $ python -m webcounter

### Local test
    
    $ python -m pytest tests/

### Coverage Report

    coverage run --source=webcounter -m pytest tests
    coverage report -m

---
## Manual server operations

### Build
    docker build -t davidniinepuu/webcounter:latest .

### Run Dependencies
    docker run -d  -p 6379:6379 --name redis --rm redis:alpine

### Deploy
    docker run -d --rm -p 80:5000 --name webcounter --link redis -e REDIS_URL=redis davidniinepuu/webcounter:latest

---
## Cluster operations

### Push to docker repository

    docker login 
    docker push davidniinepuu/webcounter:latest

### Create a docker swarm cluster

    docker swarm init

### Deploy app

    docker stack deploy --compose-file docker-compose.yml app

---
## Automated operations CI/CD


### GitLab Variables 
Variables are in: `Gitlab » Settings » CI/CD » Variables`

Create Gitlab variables with docker credentials: $USER  $PASSWORD
 
### Instal gitlab-runner

    sudo apk add gitlab-runner

### Gitlab register

Token are in: `Gitlab » Settings » CI/CD » Runners`

Add shell executer for production

    gitlab-runner register -n \
    --url https://gitlab.com/ \
    --executor shell \
    --description "docker-playground" \
    --tag-list "production-server" \
    --registration-token GR1348941ScnSJjSKuy4LPcu24_Xc

Add docker executer for tests

    gitlab-runner register -n \
    --url https://gitlab.com/ \
    --executor docker \
    --docker-image "python:3.10-alpine" \
    --description "docker-playground" \
    --tag-list "test-server" \
    --registration-token GR1348941ScnSJjSKuy4LPcu24_Xc

### Run runner

    gitlab-runner run
